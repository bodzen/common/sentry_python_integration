#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
    Custom Exception Master class, triggering sentry.io
"""

from abc import ABC, abstractmethod
import sentry_sdk


class CustomSentryException(ABC, Exception):
    def __init__(self, *args: tuple) -> None:
        error_msg = self.build_error_msg(*args)
        self.__trigger_sentry(error_msg)
        super().__init__(error_msg)

    def __trigger_sentry(self, error_msg):
        """
            Let Sentry capture the current exception
        """
        sentry_sdk.capture_exception(self)

    @abstractmethod
    def build_error_msg(self):
        pass
