#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
from typing import NoReturn

import sentry_sdk
from sentry_sdk.integrations.celery import CeleryIntegration


__all__ = ['init_sentry']


def init_sentry() -> NoReturn:
    uri = os.getenv('SENTRY_URI')
    if uri is not None:
        sentry_sdk.init(uri, integrations=[CeleryIntegration()])
        print('[+] Connection to Sentry\'s servers have been initialized')
    else:
        print('[-] SENTRY_URI env var not found. Connection to Sentry not established.')
