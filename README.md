# Sentry - Python Integration

Basic Sentry integration, to use in related project type.

![Sentry module](https://gitlab.com/dh4rm4/images-stocks/-/raw/master/spaces/spaceships/sensor_01.jpg)


## Supported project type
* Celery tasks
* Flask apps

## How To
1. Add project dependancie in requirements.txt
```
-e git+ssh://git@gitlab.com/bodzen/common/sentry_python_integration.git#egg=sentry_python_integration
```
2. a. Init sentry in main code (not in Celeryconfig, or taht will create a recursive import with celery)
```
from sentry_python_integration.PROJECT_TYPE import init_sentry
init_sentry()
```
2. b. Inehrit custom exception
```
from sentry_python_integration.common import CustomSentryException
class YoutubeError_MissingContent(CustomSentryException):
    def build_error_msg(self) -> str:
        return 'Content is not available.'

```
### Continuous Deployement
3. Gitlab variables
Create a variable SENTRY_URI_B64 containing the project related URI.
The value must be base64 encoded to suit kubernetes secret creation.
```
base64 <<< $SENTRY_URI | tr -d '\n'
```
4. Add secret creation in releated CD project.
As of Helm3, secret creation are not well managed (see [#4824](https://github.com/helm/helm/issues/4824)), forcing us to create it manually before executing the deployement through Helm.
In case of a project using Sentry, just use the Gitlab-ci template as show bellow:
```
stages:
  - deploy

include:
  - project: 'bodzen/ci-playbooks/helm'
    ref: master
	file: 'deploy_with_sentry_secret.yaml'

```
5. Add secret to mount in Helm's values.yaml
```
customSecrets:
  - name: sentry-credentials
    secret:
      secretName: PROJECT_NAME-sentry-uri

customSecretsMounts:
  - name: sentry-credentials
    mountPath: /etc/sentry_credentials/
```
